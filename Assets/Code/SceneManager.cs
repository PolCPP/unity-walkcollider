﻿using UnityEngine;

public class SceneManager : MonoBehaviour {

    public static SceneManager instance { get; private set; }
	public AudioClip defaultWalkSound;
	public PlayerController player;
	public LayerMask groundLayer;
	public float groundDist = 5f;

	void Awake() {
		instance = this;
	}
	
	public void StartWalking() {
		player.Walk();		
	}
	
	public void StopWalking() {
		player.Stop();
	}	
}