using UnityEngine;

public class ParticleSoundGroundCollider : GroundCollider {
	
	public int walkClipId;
	public int walkParticleId;
	public ParticleSystem walkParticles;
			
	public override AudioClip GetSound() { return LevelAssets.instance.walkingSounds[walkClipId]; }

	public override void TriggerCollision(Transform sourceGO) {
		foreach (Transform child in sourceGO)
		     SimplePool.Despawn(child.gameObject);
		GameObject charParticle = SimplePool.Spawn(LevelAssets.instance.walkingParticles[walkParticleId].gameObject, sourceGO.position, sourceGO.rotation);
		charParticle.transform.parent = sourceGO;
	}
}