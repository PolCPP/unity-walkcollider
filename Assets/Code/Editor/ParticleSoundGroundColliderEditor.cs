using UnityEditor;

[CustomEditor(typeof(ParticleSoundGroundCollider), true)]
public class ParticleGroundColliderEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        LevelAssets assetLib = (LevelAssets)FindObjectOfType(typeof(LevelAssets));
        ParticleSoundGroundCollider groundCol = (ParticleSoundGroundCollider)target;
        string[] soundOptions = new string[assetLib.walkingSounds.Length];
        string[] particleOptions = new string[assetLib.walkingParticles.Length];        
        for (int i = 0; i < assetLib.walkingSounds.Length; i++)
            soundOptions[i] = assetLib.walkingSounds[i].name;
        for (int i = 0; i < assetLib.walkingParticles.Length; i++)
            particleOptions[i] = assetLib.walkingParticles[i].name;            
        groundCol.walkClipId = EditorGUILayout.Popup("Walking Sound", groundCol.walkClipId, soundOptions); 
        groundCol.walkParticleId = EditorGUILayout.Popup("Walking Particles", groundCol.walkClipId, particleOptions);         
    }
}