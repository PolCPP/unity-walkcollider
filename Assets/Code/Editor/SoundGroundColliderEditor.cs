using UnityEditor;

[CustomEditor(typeof(SoundGroundCollider), true)]
public class GroundColliderEditor : Editor 
{
    public override void OnInspectorGUI()
    {
        LevelAssets sounds = (LevelAssets)FindObjectOfType(typeof(LevelAssets));
        SoundGroundCollider groundCol = (SoundGroundCollider)target;
        string[] options = new string[sounds.walkingSounds.Length];
        for (int i = 0; i < sounds.walkingSounds.Length; i++)
            options[i] = sounds.walkingSounds[i].name;
        groundCol.walkClipId = EditorGUILayout.Popup("Walking Sound", groundCol.walkClipId, options); 
    }
}