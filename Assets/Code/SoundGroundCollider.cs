using UnityEngine;

public class SoundGroundCollider : GroundCollider {
	
	public int walkClipId;
			
	public override AudioClip GetSound() { return LevelAssets.instance.walkingSounds[walkClipId]; }

	public override void TriggerCollision(Transform sourceGO) {
		 foreach (Transform child in sourceGO)
		     SimplePool.Despawn(child.gameObject);
	}
}