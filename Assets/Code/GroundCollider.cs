using UnityEngine;

public abstract class GroundCollider : MonoBehaviour 
{
	public int testVal;
	
	public abstract AudioClip GetSound();
	public abstract void TriggerCollision(Transform sourceGO);			
}