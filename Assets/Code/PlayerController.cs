using UnityEngine;

public class PlayerController : MonoBehaviour {

	public Animation characterAnim;
	public AudioSource audioSource;
	private Transform thistransform;
	public Transform walkParticleParent;

	private bool isWalking = false;
	private AudioClip newWalkClip;
	private RaycastHit groundHit; 
	int lastObjectId;
	GroundCollider groundCol;
		
	void Awake() {
		setupComponents();
	}
	
	void setupComponents() {
		thistransform = this.transform;
	}
	
	void Update() {
		if (isWalking)
			CheckGround();	
	}
	
	public void Walk() {
		isWalking = true;
		characterAnim.Play();
		audioSource.Play();		
	}
	
	public void Stop() {
		isWalking = false;
		characterAnim.Stop();
		audioSource.Stop();
		foreach (Transform child in walkParticleParent)
			GameObject.Destroy(child.gameObject);		
	}
	
	void CheckGround() {
		if (Physics.Raycast(thistransform.position, Vector3.down, 
							out groundHit, SceneManager.instance.groundDist, 
							SceneManager.instance.groundLayer)) {			
			if (lastObjectId != groundHit.collider.GetInstanceID()) {
				lastObjectId = groundHit.collider.GetInstanceID();
				groundCol = groundHit.collider.GetComponent<GroundCollider>();	
				groundCol.TriggerCollision(walkParticleParent);
				if (groundCol.GetSound() != audioSource.clip) {
					UpdateWalkClip(groundCol.GetSound());			
				}
			}
		} else {
			if (lastObjectId != -1) {
				lastObjectId = -1;				
				if (SceneManager.instance.defaultWalkSound != audioSource.clip) 
					UpdateWalkClip(SceneManager.instance.defaultWalkSound);					 
				foreach (Transform child in walkParticleParent)
				    SimplePool.Despawn(child.gameObject);						
			}	
		}
	}

	// Todo: Crossfade or something.	
	void UpdateWalkClip(AudioClip newWalkClip) {
		audioSource.Stop();
		audioSource.clip = newWalkClip;
		audioSource.Play();
	}	
}
