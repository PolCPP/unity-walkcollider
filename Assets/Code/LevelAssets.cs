using UnityEngine;

public class LevelAssets : MonoBehaviour {

	public ParticleSystem[] walkingParticles;
	public AudioClip[] walkingSounds;
    public static LevelAssets instance { get; private set; }

	void Awake() {
		instance = this;
	}

}